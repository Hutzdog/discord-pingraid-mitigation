# Discord PingRaid Mitigator
##### A pristine bot providing protection against the perils of PingRaids
---
## What is a PingRaid?
A PingRaid is a relatively new kind of attack where unregistered discord bots join a server and begin mentioning random people. This plugin mitigates this by limiting the number of users that can be mentioned in a message. If you want to mention multiple people in a group, use roles.

## Setup
To build this project, you need to do a few things:

1. Install [npm](https://npm.community), [java](https://azul.com/downloads/zulu-community/)(You don't NEED ZuluJDK, but they are the distribution I use), [GNU Make](https://www.gnu.org/software/make/), and [shadow-cljs](https://github.com/thheller/shadow-cljs)
2. Run `npm i` to install the discordjs dependency
3. Inside of the `src/pingraid_mitigator` folder, you will need to create `sensitive.cljs`with 2 variables: `tok` (your fork's Discord bot token) and `maxMessages` (the maximum number of people you can mention in 1 message before triggering the bot). I hope you can understand that we kind of need to protect those pieces of information.
4. To build a JS archive that works with Node, use `make`. To run the bot on your local machine, use `make run`

## Website
https://hutzdog.gitlab.io/dpm-pages
