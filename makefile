default:
	shadow-cljs release app
	cp -r node_modules release
	tar -czvf release.tar.xz release
	rm -rf release

run:
	shadow-cljs compile dev
	node main.js

clean:
	rm -rf main.js out

commit:
	git add .
	git commit -m "$(message)"

push:
	git push

git: commit push
