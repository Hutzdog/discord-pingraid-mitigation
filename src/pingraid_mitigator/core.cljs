(ns pingraid-mitigator.core
  (:require ["discord.js" :as discord]
            [pingraid-mitigator.sensitive :as sensitive]))

;; Config
(def client (discord/Client.))

;; Mentions atom
(def mentions (atom {}))
(defn -update-mentions [input message]
  (let [author-uname (.. message -author -username)
        mentions-len (.. message -mentions -users (array) -length)]

    (as-> input cur
        (if (<= 1 mentions-len) (assoc-in cur [(keyword author-uname) :cooldown] sensitive/messageCooldown) cur)
        (cond
          (> 1 mentions-len) cur
          (nil? (get-in cur [(keyword author-uname) :pings])) (assoc-in cur [(keyword author-uname) :pings] mentions-len)
          :else (update-in cur [(keyword author-uname) :pings] + mentions-len))
        (zipmap (keys cur) (map #(update % :cooldown dec) (vals cur)))
        (into {} (filter #(< 0 (get (second %) :cooldown)) cur)))))

(defn -main [& args]
  (.on client "ready" #(println "PRM started!"))

  (.on client "message"
       (fn [message]
         (reset! mentions (-update-mentions @mentions message))

         (cond
           (= (. message -content) "prm>ping")
           (.. message -channel (send "Pong"))

           (.. message -content (startsWith "!clear"))
           (let [clearNum (-> message (.. -content (split " ")) (nth 1))]
             (cond
               (not (.. message -member (hasPermission "MANAGE_MESSAGES")))
               (do (.. message -channel
                       (send "Sorry, but you don't have permission to do that!"))
                   (. message delete))

               (js/isNaN clearNum)
               (do (.. message -channel (send (str "Invalid number: " clearNum)))
                   (. message delete))

               :else
               (.. message -channel (bulkDelete (+ (js/parseInt clearNum 10) 1)))))

           (> (get-in @mentions [(keyword (.. message -author -username)) :pings]) sensitive/maxMessages)
           (do
             (reset! mentions (#(assoc-in % [(keyword (.. message -author -username)) :pings] 0)))
             (-> (.. message -member (send "You have tripped this server's ping raid protection."))
                 (.then (js/setTimeout #(.. message -member kick) 500))
                 (.catch (println "Err: Promise rejection handled")))))))

  ;; Login
  (.login client sensitive/tok))
